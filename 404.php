<?php
global $wp_query;
$args = array_merge($wp_query->query_vars, array('pagename' => 'error-404'));
query_posts($args);
?>

<?php get_header(); ?>

<?php if (have_rows('page_modules')) : ?>
  <?php while (have_rows('page_modules')) : the_row(); ?>

    <?php get_template_part('modules/_' . get_row_layout()); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

<?php wp_reset_query(); ?>
