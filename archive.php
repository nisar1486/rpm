<?php get_header(); ?>

<div class="wrap">

  <div class="blog">

    <div class="main">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="blog__item">
          <div class="blog__date">
            <?php the_time('F j, Y'); ?>
          </div>
          <h2 class="blog__title">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h2>
          <div class="blog__content">
            <?php echo excerpt(64, true); ?>
          </div>
        </div>
      <?php endwhile; endif; ?>
      <div class="blog__nav">
        <div class="blog__prev"><?php previous_posts_link(); ?></div>
        <div class="blog__next"><?php next_posts_link(); ?></div>
      </div>
    </div>

    <?php get_sidebar(); ?>

  </div>

</div>

<?php get_footer(); ?>
