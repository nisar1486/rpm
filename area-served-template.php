<?php 
	/* Template Name: Area Served Page Template */ 
	get_header(); 

?>
<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="locations">
  <div class="wrap">
    <h2 class="section__heading"><?php the_field('area_served_heading'); ?></h2>
    <div class="locations__down"><?php svgstore('angle-down'); ?></div>
    <ul class="locations__list">
      <?php
      $locations = get_terms('location', array(
        'hide_empty' => false
      ));
      $page_url =  get_page_url_by_template('template-area-served.php');
      foreach ($locations as $location) {
        echo '<li class="locations__item">';
        echo '<a class="locations__link" href="' . $page_url . $location->slug. '">' . $location->name . '</a>';
        echo '</li>';
      }
      ?>
    </ul>
    <a class="locations__more" href="#">More locations</a>
  </div>
</div>

<?php get_footer(); ?>
<script type="text/javascript">
  var more = document.querySelector('.locations__more');
  var locations = document.querySelector('.locations');
  if (more) {
    more.addEventListener('click', function(e) {
      locations.classList.add('locations--active');
      e.preventDefault();
    });
  }
</script>