<?php 
	global $wpdb, $wp_query;
    $lessons_terms     = array();
    $location_term     = $wp_query->query_vars['alocation'];
    $term_info         = get_term_by('slug', $location_term, 'location');
	$term_id           = $term_info->term_id;

    get_header(); 

    if (have_rows('before_content_modules', 'location_' . $term_id)) : 
        while (have_rows('before_content_modules', 'location_' . $term_id)) : the_row();
            get_template_part(
                                'modules/locations/_' . get_row_layout(), 
                                null, 
                                $term_info
                            );
        endwhile;
    endif;
?>
<!--

<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="locations">
  <div class="wrap">
    <h2 class="section__heading">Music Lessons in <?php echo $term_info->name; ?></h2>
    <ul class="locations__list">
    <?php
        $args = array(
                'post_type' => 'teacher',
                'tax_query' => array(
                    array(
                        'taxonomy'  => 'location',
                        'field'     => 'slug',
                        'terms'     => array( $location_term )
                    )
                )
            );
        $query = new WP_Query( $args );

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
                $lessons = get_the_terms( get_the_ID(), 'lesson' );
                foreach ($lessons as $lesson) {
                    $lessons_terms[$lesson->slug] = $lesson->name;
                }
            }
            wp_reset_postdata();

            foreach ($lessons_terms as $slug => $name) {
                echo '<li class="locations__item">';
                echo '<a class="locations__link" href="' . $slug. '">' . $name . '</a>';
                echo '</li>';
            }
        }
    ?>
    </ul>
  </div>
</div>
-->


<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="locations-grid">
    <h2 class="section__heading">Music Lessons in <?php echo $term_info->name; ?></h2>
    <div class="grid">
        <?php
            $args = array(
                    'post_type' => 'teacher',
                    'tax_query' => array(
                        array(
                            'taxonomy'  => 'location',
                            'field'     => 'slug',
                            'terms'     => array( $location_term )
                        )
                    )
                );
            $query = new WP_Query( $args );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    $lessons = get_the_terms( get_the_ID(), 'lesson' );
                    foreach ($lessons as $lesson) {
                        $lessons_terms[$lesson->term_id] = $lesson;
                    }
                }
                wp_reset_postdata();

                foreach ( $lessons_terms as $id => $single_lesson ) {
                ?>
                    <div class="grid-item">
                        <img src="<?php echo get_field('image', "lesson_".$id); ?>" alt="Image 1">
                        <h3 class="title"><?php echo $single_lesson->name; ?></h3>
                        <p class="description"><?php echo $single_lesson->description; ?></p>
                    </div>
                <?php
                }
            }
        ?>
    </div>
  </div>
</div>

<?php 
    if (have_rows('after_content_modules', 'location_' . $term_id)) : 
        while (have_rows('after_content_modules', 'location_' . $term_id)) : the_row(); 
            get_template_part(
                                'modules/locations/_' . get_row_layout(), 
                                null, 
                                $term_info
                            );
        endwhile;
    endif;
    get_footer(); 
?>
