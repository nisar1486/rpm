<?php 
    global $wpdb, $wp_query;
    $location_term      = $wp_query->query_vars['alocation'];
    $lesson_term        = $wp_query->query_vars['alesson'];
    $lesson_term_info   = get_term_by('slug', $lesson_term, 'lesson');
    $location_term_info = get_term_by('slug', $location_term, 'location');
    $term_id            = $lesson_term_info->term_id;
    $default_image      =  get_stylesheet_directory_uri()."/modules/locations/default.jpg";
    get_header(); 

    /*
        echo "<pre>";
        print_r($lesson_term_info);
        echo "</pre>";

        echo "<pre>";
        print_r($location_term_info);
        echo "</pre>";
    */

    if (have_rows('before_content_modules', 'lesson_' . $term_id)) : 
        while (have_rows('before_content_modules', 'lesson_' . $term_id)) : the_row();
            //echo 'modules/lessons/_' . get_row_layout();
            get_template_part(
                                'modules/lessons/_' . get_row_layout(), 
                                null, 
                                $lesson_term_info
                            );
        endwhile;
    endif;

?>

    <div class="locations_carousel"> 
        <h2 class="section__heading">Music Teachers in <?php echo $location_term_info->name; ?></h2>
        <p>Red Pelican Music offers the finest private music lessons in the <b><?php echo $location_term_info->name; ?></b> area. Learn from hand-picked, world-class teachers in the comfort of your home, teacher’s studio, or with live online lessons (virtual lessons via Zoom).</p>
        <p>Our teachers develop their own curriculum, to fit the unique goals and interests of each student. Our goal is to provide comprehensive music education, but in a fun way. We look for ways of integrating music theory into real world examples, incorporating the stylistic interests of the student.</p>
    </div>    

    <div class="carousel">
        <?php 
        $teachers = get_posts(array (
            'post_type'       => 'teacher',
            'posts_per_page'  => '-1',
            'orderby'         => 'rand',
            'tax_query'         => array(
                                        array(
                                            'taxonomy' => 'location',
                                            'field' => 'slug',
                                            'terms' => array( $location_term_info->slug )
                                        ),
                                        array(
                                            'taxonomy' => 'lesson',
                                            'field' => 'slug',
                                            'terms' => array( $lesson_term_info->slug )
                                        )
                                    )
        ));
        ?>
        <?php foreach ($teachers as $post) : setup_postdata($post); ?>
        <a class="carousel__item" href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail('carousel'); ?>
          <span class="carousel__overlay">
            <span class="carousel__overlay__top"><?php the_title(); ?></span>
            <span class="carousel__overlay__bottom">
              <span class="carousel__overlay__text"><?php echo excerpt(8); ?></span>
            </span>
          </span>
        </a>
        <?php endforeach; wp_reset_postdata(); ?>
    </div>

<?php 
    if (have_rows('after_content_modules', 'lesson_' . $term_id)) : 
        while (have_rows('after_content_modules', 'lesson_' . $term_id)) : the_row(); 
            //echo 'modules/lessons/_' . get_row_layout();
            get_template_part(
                                'modules/lessons/_' . get_row_layout(), 
                                null, 
                                $lesson_term_info
                            );
        endwhile;
    endif;
    get_footer(); 
?>