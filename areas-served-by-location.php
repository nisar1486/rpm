<?php 
    global $wpdb, $wp_query;
    $lessons_terms     = array();
    $location_term     = $wp_query->query_vars['alocation'];
    $term_info         = get_term_by('slug', $location_term, 'location');
    $term_id           = $term_info->term_id;
    $default_image     =  get_stylesheet_directory_uri()."/modules/locations/default.jpg";

    get_header(); 

    if (have_rows('before_content_modules', 'location_' . $term_id)) : 
        while (have_rows('before_content_modules', 'location_' . $term_id)) : the_row();
            get_template_part(
                                'modules/locations/_' . get_row_layout(), 
                                null, 
                                $term_info
                            );
        endwhile;
    endif;
?>

<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="locations-grid">
    <h2 class="section__heading">Music Lessons in <?php echo $term_info->name; ?></h2>
    <div class="grid-row">
        <?php
            $args = array(
                        'taxonomy' => 'lesson',
                    );
            $lessons_terms = get_terms($args);

            foreach ( $lessons_terms as $id => $single_lesson ) {
                $show_on_location_page = get_field('show_on_location_page', "lesson_".$single_lesson->term_id);
                
                if( !is_array( $show_on_location_page ) ){
                    $show_on_location_page = array();
                }

                if( !in_array($term_id, $show_on_location_page) ){
                    continue;
                }

                $image = get_field('image', "lesson_".$single_lesson->term_id);
                if(empty($image)){
                    $image = $default_image;
                }
            ?>
            <div class="grid-item">
                <a class="wrapping-link" href="<?php echo $single_lesson->slug; ?>"></a>
                <div class="grid-item-wrapper">
                    <div class="grid-item-container">
                      <div class="grid-image-top rex-ray">
                        <!--<span class="centered project-image-bg rex-ray-image" sty></span>-->
                        <img src="<?php echo $image ; ?>" class="centered project-image-bg rex-ray-image">
                      </div>
                      <div class="grid-item-content">
                        <span class="item-title"><?php echo $single_lesson->name; ?></span>
                        <span class="item-excerpt"><?php echo $single_lesson->description; ?></span>
                      </div>
                    </div>
                </div>
            </div>
            <?php
            }
        ?>
    </div>
  </div>
</div>

<?php 
    if (have_rows('after_content_modules', 'location_' . $term_id)) : 
        while (have_rows('after_content_modules', 'location_' . $term_id)) : the_row(); 
            get_template_part(
                                'modules/locations/_' . get_row_layout(), 
                                null, 
                                $term_info
                            );
        endwhile;
    endif;
    get_footer(); 
?>
