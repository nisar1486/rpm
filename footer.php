    <?php 
    global $post;
    $post_slug = $post->post_name;
    if (have_rows('footer_modules', 'option') &&  $post_slug != 'areas-served' ) : ?>
      <?php while (have_rows('footer_modules', 'option')) : the_row(); ?>

        <?php get_template_part('modules/_' . get_row_layout()); ?>

      <?php endwhile; ?>
    <?php endif; ?>
    
    <footer class="footer">
      <div class="footer__top">
        <div class="wrap">
          <div class="footer__top__wrap">
            <div class="footer__lessons">
              <div class="footer__heading">Popular Lessons</div>
              <ul class="footer__nav">
                <?php
                $lessons = get_terms('lesson', array(
                  'hide_empty' => false
                ));
                foreach ($lessons as $lesson) {
                  echo '<li class="footer__nav__item--half">';
                  echo '<a href="' . get_term_link($lesson) . '">' . $lesson->name . '</a>';
                  echo '</li>';
                }
                ?>
              </ul>
            </div>
            <div class="footer__section">
              <div class="footer__heading">Sitemap</div>
              <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'sitemap',
                  'container' => false,
                  'menu_class' => 'footer__nav'
                )
              );
              ?>
              <div class="footer__heading">More</div>
              <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'more',
                  'container' => false,
                  'menu_class' => 'footer__nav'
                )
              );
              ?>
            </div>
            <div class="footer__section">
              <div class="footer__heading">About</div>
              <div class="footer__contact">
                <div class="footer__contact__title">Red Pelican Music</div>
                <?php if (get_field('footer_address', 'options')) : ?>
                  <a class="footer__contact__item" href="<?php the_field('footer_address_link', 'options'); ?>">
                    <span class="footer__contact__icon"><?php svgstore('home'); ?></span>
                    <?php the_field('footer_address', 'options'); ?>
                  </a>
                <?php endif; ?>
                <a class="footer__contact__item" href="tel:+13104399798">
                  <span class="footer__contact__icon">
                  <?php svgstore('phone'); ?></span>(310) 439-9798
                </a>
                <a class="footer__contact__item" href="mailto:team@redpelicanmusic.com">
                  <span class="footer__contact__icon">
                  <?php svgstore('envelope'); ?></span>team@redpelicanmusic.com
                </a>
              </div>
              <ul class="footer__social">
                <li class="footer__social__item">
                  <a class="footer__social__link" href="https://www.facebook.com/redpelicanmusic"><?php svgstore('facebook'); ?></a>
                </li>
                <li class="footer__social__item">
                  <a class="footer__social__link" href="https://twitter.com/redpelicanmusic"><?php svgstore('twitter'); ?></a>
                </li>
                <li class="footer__social__item">
                  <a class="footer__social__link" href="https://www.youtube.com/user/redpelicanmusic"><?php svgstore('youtube'); ?></a>
                </li>
                <li class="footer__social__item">
                  <a class="footer__social__link" href="https://www.yelp.com/biz/red-pelican-music-los-angeles-2"><?php svgstore('yelp'); ?></a>
                </li>
                <li class="footer__social__item">
                  <a class="footer__social__link" href="https://goo.gl/maps/LjNYQa5xDuP2"><?php svgstore('google'); ?></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="footer__bottom">
        <div class="wrap footer__bottom__wrap">
          <div class="footer__copyright">
            &copy; <?php echo date('Y'); ?> Red Pelican LLC. All Rights Reserved.
          </div>
          <a class="footer__button footer__button--call" href="tel:+13104399798">
            <span class="footer__button__text">call us</span>
            <span class="footer__button__image"><?php svgstore('phone'); ?></span>
          </a>
          <a class="footer__button footer__button--email" href="<?php the_field('footer_message_url', 'option'); ?>">
            <span class="footer__button__text">send us a message</span>
            <span class="footer__button__image"><?php svgstore('envelope'); ?></span>
          </a>
        </div>
      </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>
