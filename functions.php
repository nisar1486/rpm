<?php

// theme css
function theme_css() {
  wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Maven+Pro:400,700%7cCrimson+Text:400,400italic', null, null);
  wp_enqueue_style('theme', get_template_directory_uri() . '/dist/css/style.css', null, null);
}
add_action('wp_enqueue_scripts', 'theme_css');

// theme js
function theme_js() {
  wp_enqueue_script('polyfill', 'https://cdn.polyfill.io/v2/polyfill.min.js', array(), null, true);
  wp_enqueue_script('theme', get_template_directory_uri() . '/dist/js/script.js', array(), null, true);
}
add_action('wp_enqueue_scripts', 'theme_js');

// theme editor css
function theme_editor_css() {
  add_editor_style('dist/css/style-editor.css');
}
add_action('admin_init', 'theme_editor_css');

// add featured images support
add_theme_support('post-thumbnails');

// image sizes
add_image_size('splash', 1920, 540, true);
add_image_size('splash-large', 1920, 1080, true);
add_image_size('gallery-full', 768, 576, true);
add_image_size('gallery-nav', 192, 144, true);
add_image_size('why', 624, 9999, false);
add_image_size('review', 130, 130, true);
add_image_size('teacher', 200, 200, true);
add_image_size('carousel', 300, 300, true);

// disable automatic media link
function disable_automatic_media_link() {
  $image_set = get_option('image_default_link_type');
  if ($image_set !== 'none') {
    update_option('image_default_link_type', 'none');
  }
}
add_action('admin_init', 'disable_automatic_media_link', 10);

// remove caption padding
function remove_caption_padding($width) {
  return $width - 10;
}
add_filter('img_caption_shortcode_width', 'remove_caption_padding');

// get top level id
function get_top_level_id() {
  global $post;
  $current_id = $post->ID;
  $ancestor_id = array_pop(get_post_ancestors($current_id));
  if ($ancestor_id) {
    return $ancestor_id;
  } else {
    return $current_id;
  }
}

// default excerpt length
function new_excerpt_length($length) {
  return 128;
}
add_filter('excerpt_length', 'new_excerpt_length');

// custom excerpt function
function custom_read_more() {
    return '... <a href="' . get_permalink(get_the_ID()) . '">Read more</a>';
}
function excerpt($limit, $more = false, $data = null) {
  if (!has_excerpt()) {
    if ($data == null) {
      $data = get_the_excerpt();
    }
    if ($more == true) {
      return wp_trim_words($data, $limit, custom_read_more());
    } else {
      return wp_trim_words($data, $limit);
    }
  } else {
    return get_the_excerpt();
  }
}

// svgstore
function svgstore($svg) {
  echo '
    <span class="svgstore svgstore--' . $svg . '">
      <svg>
        <use xlink:href="' . get_template_directory_uri() . '/dist/img/svgstore.svg#' . $svg . '"></use>
      </svg>
    </span>
  ';
}

// custom nav menus
register_nav_menus(
  array(
    'header' => 'Header',
    'sitemap' => 'Sitemap',
    'more' => 'More'
  )
);

// change yoast seo metabox priority
add_filter('wpseo_metabox_prio', function() { return 'low'; });

// Register Custom Taxonomy
function custom_taxonomy_lesson() {

  $labels = array(
    'name'                       => _x( 'Lessons', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Lesson', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Lessons', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                       => 'lessons/los-angeles',
    'with_front'                 => true,
    'hierarchical'               => false,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    'rewrite'                    => $rewrite,
  );
  register_taxonomy( 'lesson', array( 'teacher' ), $args );

}
add_action( 'init', 'custom_taxonomy_lesson', 0 );

// Register Custom Taxonomy
function custom_taxonomy_location() {

	$labels = array(
		'name'                       => _x( 'Locations', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Locations', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
  $rewrite = array(
    'slug'                       => 'locations',
    'with_front'                 => true,
    'hierarchical'               => false,
  );
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
    'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'location', array( 'teacher' ), $args );

}
add_action( 'init', 'custom_taxonomy_location', 0 );

// Register Custom Post Type
function custom_post_type_teacher() {

  $labels = array(
    'name'                  => _x( 'Teachers', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Teacher', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Teachers', 'text_domain' ),
    'name_admin_bar'        => __( 'Teacher', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                  => 'teachers',
    'with_front'            => true,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __( 'Teacher', 'text_domain' ),
    'description'           => __( 'Red Pelican Music teachers', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', ),
    'taxonomies'            => array( 'location', 'lesson' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-format-audio',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
    'capability_type'       => 'post',
  );
  register_post_type( 'teacher', $args );

}
add_action( 'init', 'custom_post_type_teacher', 0 );

// Register Custom Post Type
function custom_post_type_review() {

  $labels = array(
    'name'                  => _x( 'Reviews', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Review', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Reviews', 'text_domain' ),
    'name_admin_bar'        => __( 'Review', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                  => 'reviews',
    'with_front'            => true,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __( 'Review', 'text_domain' ),
    'description'           => __( 'Red Pelican Music reviews', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-thumbs-up',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
    'capability_type'       => 'post',
  );
  register_post_type( 'review', $args );

}
add_action( 'init', 'custom_post_type_review', 0 );

// only show acf on local dev
function hide_acf_menu_item() {
  return !(strpos(get_bloginfo('url'), 'localhost') === false);
}
//add_filter('acf/settings/show_admin', 'hide_acf_menu_item');

// add acf options page
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array (
    'page_title' => 'Theme Settings',
    'menu_title' => 'Theme Settings',
    'menu_slug' => 'theme-settings'
  ));
}

// custom rewrite rules for teacher post type
function custom_rewrite_teacher() {
  //add_rewrite_rule('^lessons/([a-zA-Z0-9-_]+)/([a-zA-Z0-9-_]+)/?', 'index.php?post_type=teacher&location=$matches[1]&lesson=$matches[2]', 'top');
}
add_action('init', 'custom_rewrite_teacher');

// widgetize sidebar
if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Blog Sidebar',
    'id' => 'sidebar-blog'
  ));
}


// -------------------------------------

 // Edit below this line

 // -------------------------------------
function rpm_query_vars_callback($vars) {
    array_push($vars, 'alocation');
    array_push($vars, 'alesson');
    return $vars;
}

add_filter('query_vars','rpm_query_vars_callback');

function rpm_rewrite_callback() {

    add_rewrite_rule( '^areas-served/([^/]*)/([^/]*)/?', 'index.php?pagename=areas-served&alocation=$matches[1]&alesson=$matches[2]', 'top' );
    add_rewrite_rule( '^areas-served/([^/]*)/?', 'index.php?pagename=areas-served&alocation=$matches[1]', 'top' );

    add_rewrite_tag('%alocation%','([^&]+)');
    add_rewrite_tag('%alesson%','([^&]+)');

}

add_action('init', 'rpm_rewrite_callback');

function rpm_templates_callback($template) {

    global $wp_query;

    if (isset($wp_query->query_vars['alocation']) && isset($wp_query->query_vars['alesson'])) {
        return  dirname(__FILE__) . '/areas-served-by-location-lesson.php';
    } elseif (isset($wp_query->query_vars['alocation'])) {
        return dirname(__FILE__) . '/areas-served-by-location.php';
    }

    return $template;
}

add_filter('template_include', 'rpm_templates_callback', 1, 1);


function get_page_url_by_template( $TEMPLATE_NAME ) {
    $pages = query_posts( [
        'post_type'  => 'page',
        'meta_key'   => '_wp_page_template',
        'meta_value' => $TEMPLATE_NAME
    ] );
    $url   = '';
    if ( isset( $pages[0] ) ) {
        $array = (array) $pages[0];
        $url   = get_page_link( $array['ID'] );
    }

    return $url;
}