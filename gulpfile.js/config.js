// browserify transforms
var requireGlobify = require('require-globify');

// postcss plugins
var autoprefixer = require('autoprefixer');

// configuration for gulp tasks
module.exports = {
  browserSync: {
    opts: {
      proxy: 'redpelicanmusic.localhost',
      open: 'ui'
    }
  },
  browserify: {
    src: 'src/js/script.js',
    bundle: 'script.js',
    dist: 'dist/js',
    watch: 'src/js',
    resave: 'src/js/script.js',
    opts: {
      cache: {},
      packageCache: {},
      debug: true,
      transform: [requireGlobify]
    }
  },
  clean: {
    target: ['dist']
  },
  images: {
    src: 'src/img/**/*',
    dist: 'dist/img'
  },
  sass: {
    src: 'src/sass/**/*.scss',
    dist: 'dist/css',
    postcss: [autoprefixer()]
  },
  svgstore: {
    src: 'src/svgstore/**/*.svg',
    dist: 'dist/img',
    template: 'src/svgstore/util/_template.mustache',
    sass: '_svgstore.scss',
    out: 'src/sass/util',
    opts: {
      inlineSvg: true
    },
    imagemin: {
      plugins: [
        {removeTitle: true},
        {removeDimensions: true},
        {removeViewBox: false},
      ]
    }
  },
};
