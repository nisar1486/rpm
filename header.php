<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <title><?php wp_title(''); ?></title>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header class="header">
      <div class="wrap">
        <a class="header__logo" href="<?php bloginfo('url'); ?>">
          <span class="header__logo__image"><?php svgstore('logo'); ?></span>
          <span class="header__logo__text">Red Pelican Music</span>
        </a>
        <a class="header__nav-toggle" href="#">
          <span class="header__nav-toggle__image"><?php svgstore('bars'); ?></span>
          <span class="header__nav-toggle__text">Navigation</span>
        </a>
        <?php
        wp_nav_menu(
          array(
            'theme_location' => 'header',
            'container' => false,
            'menu_class' => 'header__nav'
          )
        );
        ?>
        <a class="header__call" href="tel:+13104399798">
          <span class="header__call__image"><?php svgstore('phone'); ?></span>
          <span class="header__call__text">(310) 439-9798</span>
        </a>
      </div>
    </header>
    <?php if (have_rows('header_modules', 'option')) : ?>
      <?php while (have_rows('header_modules', 'option')) : the_row(); ?>

        <?php get_template_part('modules/_' . get_row_layout()); ?>

      <?php endwhile; ?>
    <?php endif; ?>
