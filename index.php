<?php get_header(); ?>

<?php if (have_rows('page_modules')) : ?>
  <?php while (have_rows('page_modules')) : the_row(); ?>

    <?php get_template_part('modules/_' . get_row_layout()); ?>

  <?php endwhile; ?>
<?php else: ?>

  <div class="wrap">

    <div class="blog">

      <div class="main">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <div class="blog__item">
            <div class="blog__date">
              <?php the_time('F j, Y'); ?>
            </div>
            <h1 class="blog__title">
              <?php the_title(); ?>
            </h1>
            <div class="blog__content">
              <?php the_content(); ?>
            </div>
          </div>
        <?php endwhile; endif; ?>
        <div class="blog__nav">
          <div class="blog__prev"><?php previous_post_link('%link', '&laquo; %title'); ?></div>
          <div class="blog__next"><?php next_post_link('%link', '%title &raquo;'); ?></div>
        </div>
      </div>

      <?php get_sidebar(); ?>

    </div>

  </div>

<?php endif; ?>

<?php get_footer(); ?>
