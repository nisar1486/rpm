<div class="accordion">
  <?php if (have_rows('accordion')) : ?>
    <?php while (have_rows('accordion')) : the_row(); ?>
      <div class="accordion__item">
        <a class="accordion__toggle" href="#">
          <span class="accordion__toggle__icon">
            <span class="accordion__toggle__open"><?php svgstore('plus'); ?></span>
            <span class="accordion__toggle__close"><?php svgstore('minus'); ?></span>
          </span>
          <?php the_sub_field('heading'); ?>
        </a>
        <div class="accordion__content">
          <?php the_sub_field('content'); ?>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</div>
