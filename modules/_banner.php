<?php
$background = '';
if (get_sub_field('background') != 'blue') {
  $background = ' banner--' . get_sub_field('background');
}
?>

<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="banner<?php echo $background; ?>">
  <div class="wrap wrap--narrow">
    <div class="banner__container">
      <?php if (get_sub_field('subheading')) : ?>
        <div class="section__subheading"><?php the_sub_field('subheading'); ?></div>
      <?php endif; ?>
      <?php if (get_sub_field('heading_tag')) : ?>
        <<?php the_sub_field('heading_tag'); ?> class="section__heading"><?php the_sub_field('heading'); ?></<?php the_sub_field('heading_tag'); ?>>
      <?php else : ?>
        <h2 class="section__heading"><?php the_sub_field('heading'); ?></h2>
      <?php endif; ?>
      <?php the_sub_field('content'); ?>
      <?php if (get_sub_field('button_text') && get_sub_field('button_link')) : ?>
        <a class="banner__button" href="<?php the_sub_field('button_link'); ?>">
          <?php the_sub_field('button_text'); ?>
          <span class="banner__button__icon"><?php svgstore('caret-right'); ?></span>
        </a>
      <?php endif; ?>
    </div>
  </div>
</div>
