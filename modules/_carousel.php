<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="carousel">
  <?php 
  $teachers = get_posts(array (
    'post_type' => 'teacher',
    'posts_per_page' => '-1',
    'orderby' => 'rand'
  ));
  ?>
  <?php foreach ($teachers as $post) : setup_postdata($post); ?>
    <a class="carousel__item" href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail('carousel'); ?>
      <span class="carousel__overlay">
        <span class="carousel__overlay__top"><?php the_title(); ?></span>
        <span class="carousel__overlay__bottom">
          <span class="carousel__overlay__text"><?php echo excerpt(8); ?></span>
        </span>
      </span>
    </a>
  <?php endforeach; wp_reset_postdata(); ?>
</div>
