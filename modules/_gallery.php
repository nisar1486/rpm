<div class="gallery">
  <div class="gallery__full"></div>
  <ul class="gallery__nav">
    <?php if (have_rows('items')) : ?>
      <?php $i = 0; while (have_rows('items')) : the_row(); $i++; ?>
        <li class="gallery__nav__item">
          <?php
          if (get_sub_field('type') == 'video') {
            $load = '<iframe width="200" height="150" src="https://www.youtube.com/embed/'.get_sub_field('video').'" frameborder="0" allowfullscreen></iframe>';
          } else {
            $load = wp_get_attachment_image(get_sub_field('image'), 'gallery-full');
          }
          ?>
          <a class="gallery__nav__link<?php if ($i == 1) : ?> gallery__nav__link--active<?php endif; ?>" href="#" data-load='<?php echo $load; ?>'>
            <?php echo wp_get_attachment_image(get_sub_field('image'), 'gallery-nav'); ?>
            <?php if (get_sub_field('type') == 'video') : ?>
              <span class="gallery__nav__icon"><?php svgstore('play-circle'); ?></span>
            <?php endif; ?>
          </a>
        </li>
      <?php endwhile; ?>
    <?php endif; ?>
  </ul>
</div>
