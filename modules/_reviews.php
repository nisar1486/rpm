<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="reviews">
  <div class="wrap">
    <h2 class="section__heading"><?php the_sub_field('heading'); ?></h2>
    <ul class="reviews__list">
      <?php
      $reviews = get_posts(array (
        'post_type' => 'review',
        'posts_per_page' => '-1',
        'orderby' => 'rand'
      ));
      ?>
      <?php foreach ($reviews as $post) : setup_postdata($post); ?>
        <li class="reviews__item">
          <div class="wrap wrap--narrow">
            <?php the_post_thumbnail('review', array('class' => 'reviews__photo')); ?>
            <h2 class="reviews__heading"><?php the_title(); ?></h2>
            <div class="reviews__text"><?php the_content(); ?></div>
          </div>
        </li>
      <?php endforeach; wp_reset_postdata(); ?>
    </ul>
    <?php if (!get_sub_field('hide_external')) : ?>
      <div class="reviews__external">
        <div class="wrap wrap--narrow">
          <div class="reviews__score">
            <a class="reviews__score__item reviews__score__item--google" href="https://goo.gl/maps/LjNYQa5xDuP2">
              <?php svgstore('google-logo'); ?>
              <span class="reviews__score__detail">
                <?php the_field('google_rating', 'option'); ?> out of 5.0<br><?php the_field('google_reviews', 'option'); ?> reviews
              </span>
            </a>
          </div>
          <div class="reviews__score">
            <a class="reviews__score__item" href="https://www.yelp.com/biz/red-pelican-music-los-angeles-2">
              <?php svgstore('yelp-logo'); ?>
              <span class="reviews__score__detail">
                <?php the_field('yelp_rating', 'option'); ?> out of 5.0<br><?php the_field('yelp_reviews', 'option'); ?> reviews
              </span>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
