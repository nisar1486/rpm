<?php
$format = '';
$background = wp_get_attachment_image_src(get_sub_field('background'), 'splash')[0];
if (get_sub_field('format') != 'default') {
  $format = ' splash--' . get_sub_field('format');
  $background = wp_get_attachment_image_src(get_sub_field('background'), 'splash--' . get_sub_field('format'))[0];
}
?>

<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="splash<?php echo $format; ?><?php if (get_sub_field('dark_overlay')) : ?> splash--dark-overlay<?php endif; ?>" style="background-image: url(<?php echo $background; ?>);">
  <div class="wrap">
    <div class="splash__content">
      <?php if(get_sub_field('heading')) : ?>
        <?php if (get_sub_field('heading_tag')) : ?>
          <<?php the_sub_field('heading_tag'); ?> class="splash__heading"><?php the_sub_field('heading'); ?></<?php the_sub_field('heading_tag'); ?>>
        <?php else : ?>
          <h2 class="splash__heading"><?php the_sub_field('heading'); ?></h2>
        <?php endif; ?>
      <?php endif; ?>
      <?php if(get_sub_field('subheading')) : ?>
        <?php if (get_sub_field('subheading_tag')) : ?>
          <<?php the_sub_field('subheading_tag'); ?> class="splash__subheading"><?php the_sub_field('subheading'); ?></<?php the_sub_field('subheading_tag'); ?>>
        <?php else : ?>
          <h3 class="splash__subheading"><?php the_sub_field('subheading'); ?></h3>
        <?php endif; ?>
      <?php endif; ?>
      <?php if (get_sub_field('button_text') && get_sub_field('button_link')) : ?>
        <div class="splash__button">
          <a class="button button--dashed" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
          <div class="splash__arrow"><?php svgstore('arrow-left'); ?></div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
