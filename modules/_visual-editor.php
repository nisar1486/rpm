<div class="container container--<?php the_sub_field('background'); ?>">
  <div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="wrap">
    <div class="blog">
      <div class="main main--full">
        <div class="blog__content">
          <?php the_sub_field('content'); ?>
        </div>
      </div>
    </div>
  </div>
</div>
