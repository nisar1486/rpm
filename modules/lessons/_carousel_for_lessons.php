<div class="locations_carousel"> 
    <h2 class="section__heading">Music Teachers in <?php echo $args->name; ?></h2>
    <p>Red Pelican Music offers the finest private music lessons in the <b><?php echo $args->name; ?></b> area. Learn from hand-picked, world-class teachers in the comfort of your home, teacher’s studio, or with live online lessons (virtual lessons via Zoom).</p>
    <p>Our teachers develop their own curriculum, to fit the unique goals and interests of each student. Our goal is to provide comprehensive music education, but in a fun way. We look for ways of integrating music theory into real world examples, incorporating the stylistic interests of the student.</p>
</div>    

<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="carousel">
  <?php 
  $teachers = get_posts(array (
    'post_type'       => 'teacher',
    'posts_per_page'  => '-1',
    'orderby'         => 'rand',
    'tax_query'       => array(
                        array(
                            'taxonomy' => 'location',
                            'field' => 'slug',
                            'terms' => array( $args->slug )
                        ),
                       ) 
  ));
  ?>
  <?php foreach ($teachers as $post) : setup_postdata($post); ?>
    <a class="carousel__item" href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail('carousel'); ?>
      <span class="carousel__overlay">
        <span class="carousel__overlay__top"><?php the_title(); ?></span>
        <span class="carousel__overlay__bottom">
          <span class="carousel__overlay__text"><?php echo excerpt(8); ?></span>
        </span>
      </span>
    </a>
  <?php endforeach; wp_reset_postdata(); ?>
</div>
