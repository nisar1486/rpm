<section class="hero">
  <img src="<?php echo get_sub_field('image'); ?>" alt="Hero Image">
  <div class="hero-text-container">
    <h1 class="hero-title"><?php echo get_sub_field('title'); ?></h1>
    <p class="hero-description"><?php echo get_sub_field('content'); ?></p>
  </div>
</section>