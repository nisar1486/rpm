<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="phone">
  <div class="wrap">
    <a class="phone__button" href="tel:3108930776">
      <span class="phone__image"><?php svgstore('phone'); ?></span>
      <span class="phone__text"><?php the_sub_field('button_text'); ?></span>
    </a>
  </div>
</div>
