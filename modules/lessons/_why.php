<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="why">
  <div class="wrap">
    <?php if (get_sub_field('image')) : ?>
      <div class="why__container">
        <div class="why__image">
          <?php echo wp_get_attachment_image(get_sub_field('image'), 'why'); ?>
        </div>
        <div class="why__content">
    <?php endif; ?>
          <h2 class="section__heading why__heading"><?php the_sub_field('heading'); ?></h2>
          <?php get_template_part('modules/_accordion'); ?>
    <?php if (get_sub_field('image')) : ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
