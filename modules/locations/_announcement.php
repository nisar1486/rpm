<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="announcement">
  <div class="wrap">
    <?php
    if (get_sub_field('button_link')) {
      $tag = 'a';
    } else {
      $tag = 'div';
    }
    ?>
    <<?php echo $tag; ?> class="announcement__button" href="<?php the_sub_field('button_link'); ?>">
      <span class="announcement__image"><?php svgstore('bullhorn'); ?></span>
      <span class="announcement__text"><?php the_sub_field('button_text'); ?></span>
    </<?php echo $tag; ?>>
  </div>
</div>
