<?php
$id = get_queried_object_id();
$hidden = get_sub_field('hidden') ?: [];
?>

<?php if (!in_array($id, $hidden)) : ?>

  <div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="location-cta">
    <div class="wrap">
      <div class="location_cta__container">
        <h2><?php the_sub_field('cta_text'); ?></h2>
        <a class="cta__button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
      </div>
    </div>
  </div>

<?php endif; ?>
