<div class="filter">
  <div class="wrap">
    <!--
    <div class="filter__select">
      <div class="filter__select__arrow"><?php svgstore('angle-down'); ?></div>
      <div class="filter__select__text"></div>
      <select id="filter-location">
        <option value="">All locations</option>
        <?php  $locations = get_terms(array(
          'taxonomy' => 'location'
        )); ?>
        <?php foreach ($locations as $location) { ?>
          <option value="<?php echo $location->slug; ?>"<?php if (get_query_var('location') === $location->slug) echo ' selected' ?>><?php echo $location->name; ?></option>
        <?php } ?>
      </select>
    </div>
    -->
    <div class="filter__select">
      <div class="filter__select__arrow"><?php svgstore('angle-down'); ?></div>
      <div class="filter__select__text"></div>
      <select id="filter-lesson">
        <option value="">All instruments</option>
        <?php  $lessons = get_terms(array(
          'taxonomy' => 'lesson'
        )); ?>
        <?php foreach ($lessons as $lesson) { ?>
          <option value="<?php echo $lesson->slug; ?>"<?php if (get_query_var('lesson') === $lesson->slug) echo ' selected' ?>><?php echo $lesson->name; ?></option>
        <?php } ?>
      </select>
    </div>
    <a class="button filter__submit" href="#">Search</a>
  </div>
</div>
