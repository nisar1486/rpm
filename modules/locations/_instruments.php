<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="instruments">
  <div class="wrap">
    <h2 class="section__heading"><?php the_sub_field('heading'); ?></h2>
    <div class="instruments__down"><?php svgstore('angle-down'); ?></div>
    <ul class="instruments__list">
      <?php
      $lessons = get_terms('lesson', array(
        'hide_empty' => false
      ));
      foreach ($lessons as $lesson) {
        echo '<li class="instruments__item">';
        echo '<a class="instruments__link" href="' . get_term_link($lesson) . '">' . $lesson->name . '</a>';
        echo '</li>';
      }
      ?>
    </ul>
    <a class="instruments__more" href="#">More Instruments</a>
  </div>
</div>
