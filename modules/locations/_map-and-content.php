<div class="map-and-content">
	<div class="half">
		<?php echo get_sub_field('map'); ?>	
	</div>
	<div class="half">
		<?php echo get_sub_field('content'); ?>	
	</div>
</div>	