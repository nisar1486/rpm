<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="media">
  <div class="wrap">
    <div class="media__container">
      <?php if (have_rows('galleries')) : ?>
        <?php while (have_rows('galleries')) : the_row(); ?>
          <div class="media__item">
            <h2 class="media__heading"><?php the_sub_field('heading'); ?></h2>
            <?php get_template_part('modules/_gallery'); ?>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
