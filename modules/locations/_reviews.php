<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="reviews location-reviews">
  <div class="wrap">
    <h2 class="section__heading"><?php the_sub_field('heading'); ?></h2>
    <?php if (!get_sub_field('hide_external')) : ?>
      <div class="reviews__external">
        <div class="wrap wrap--narrow">
          <div class="reviews__score">
            <div data-embed-placeholder="11649"><script src="https://www.local-marketing-reports.com/external/showcase-reviews/embed/856488e0842b4cb4109b7a8c7c6cd25079baa4b4?id=11649"></script></div>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
