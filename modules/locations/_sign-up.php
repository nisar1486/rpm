<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="sign-up">
  <div class="wrap wrap--narrow">
    <div class="section__subheading"><?php the_sub_field('subheading'); ?></div>
    <h2 class="sign-up__heading section__heading">
      <?php the_sub_field('heading'); ?>
      <div class="sign-up__arrow"><?php svgstore('arrow-down'); ?></div>
    </h2>
    <?php the_sub_field('form_embed'); ?>
  </div>
</div>
