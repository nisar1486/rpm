<div<?php if (get_sub_field('id')) : ?> id="<?php the_sub_field('id'); ?>"<?php endif; ?> class="steps">
  <div class="wrap">
    <?php if (get_sub_field('subheading')) : ?>
      <div class="section__subheading"><?php the_sub_field('subheading'); ?></div>
    <?php endif; ?>
    <h2 class="section__heading"><?php the_sub_field('heading'); ?></h2>
    <div class="steps__container">
      <?php if (have_rows('items')) : ?>
        <?php while (have_rows('items')) : the_row(); ?>
          <div class="steps__item">
            <div class="steps__arrow"><?php svgstore('arrow-right'); ?></div>
            <div class="steps__icon"><?php svgstore(get_sub_field('icon')); ?></div>
            <div class="steps__heading"><?php the_sub_field('heading'); ?></div>
            <div class="steps__description"><?php the_sub_field('content'); ?></div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
