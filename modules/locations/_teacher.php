<div class="teacher">
  <div class="wrap">
    <div class="main">
      <div class="teacher__top">
        <?php if (is_singular()) : ?>
          <?php the_post_thumbnail('teacher', array('class' => 'teacher__image')); ?>
        <?php else : ?>
          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('teacher', array('class' => 'teacher__image')); ?></a>
        <?php endif; ?>
        <h2 class="teacher__heading">
          <?php if (is_singular()) : ?>
            <?php the_title(); ?>
          <?php else : ?>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          <?php endif; ?>
        </h2>
        <?php if (is_singular()) : ?>
          <?php if (have_rows('testimonials')) : ?>
            <div class="teacher__testimonials">
              <div class="teacher__testimonials__icon"><?php svgstore('quote-right'); ?></div>
              <?php while (have_rows('testimonials')) : the_row(); ?>
                <div class="teacher__testimonials__item">
                  <div class="teacher__testimonials__text"><?php the_sub_field('text'); ?></div>
                  <div class="teacher__testimonials__source"><?php the_sub_field('source'); ?></div>
                </div>
              <?php endwhile; ?>
            </div>
          <?php endif; ?>
        <?php else : ?>
          <p class="teacher__excerpt"><?php echo excerpt(72); ?></p>
        <?php endif; ?>
      </div>
      <?php if (is_singular()) : ?>
        <div class="teacher__bio">
          <?php the_content(); ?>
        </div>
      <?php endif; ?>
      <?php if (is_singular()) : ?>
        <div class="teacher__info">
          <?php if (have_rows('information')) : ?>
            <?php while (have_rows('information')) : the_row(); ?>
              <div class="teacher__info__item">
                <div class="teacher__info__icon"><?php svgstore(get_sub_field('icon')) ?></div>
                <div class="teacher__info__heading"><?php the_sub_field('heading'); ?></div>
                <div class="teacher__info__text"><?php the_sub_field('text'); ?></div>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
    <div class="sidebar">
      <div class="teacher__sidebar">
        <div class="teacher__sidebar__item">
          <div class="teacher__sidebar__heading">Location</div>
          <div class="teacher__sidebar__text"><?php echo get_the_term_list($post->ID, 'location', '', ', ', ''); ?></div>
        </div>
        <div class="teacher__sidebar__item">
          <div class="teacher__sidebar__heading">Lessons</div>
          <div class="teacher__sidebar__text lesson-names"><?php //echo get_the_term_list($post->ID, 'lesson', '', ', ', ''); ?>
		  <?php
$terms = get_the_terms( $post->ID, 'lesson' );
if ($terms && ! is_wp_error($terms)): ?>
    <?php foreach($terms as $term): ?>
        <a href="<?php echo get_term_link( $term->slug, 'lesson'); ?>" rel="tag" class="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a><span><?php echo ","; ?></span>
    <?php endforeach; ?>
<?php endif; ?>
		  </div>
        </div>
        <?php if (is_singular()) : ?>
          <?php if (have_rows('sidebar')) : ?>
            <?php while (have_rows('sidebar')) : the_row(); ?>
              <div class="teacher__sidebar__item">
                <div class="teacher__sidebar__heading"><?php the_sub_field('heading'); ?></div>
                <div class="teacher__sidebar__text"><?php the_sub_field('text'); ?></div>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php if (!is_singular()) : ?>
      <div class="main">
        <div class="teacher__more">
          <a class="button" href="<?php the_permalink(); ?>">More about <?php the_title(); ?>...</a>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
