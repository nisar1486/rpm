<?php get_header(); ?>

<?php if (have_rows('before_content_modules')) : ?>
  <?php while (have_rows('before_content_modules')) : the_row(); ?>

    <?php get_template_part('modules/_' . get_row_layout()); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('modules/_teacher'); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('after_content_modules')) : ?>
  <?php while (have_rows('after_content_modules')) : the_row(); ?>

    <?php get_template_part('modules/_' . get_row_layout()); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
