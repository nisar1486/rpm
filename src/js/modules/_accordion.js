var toggle = document.querySelectorAll('.accordion__toggle');
Array.prototype.forEach.call(toggle, function(el, i) {
  el.addEventListener('click', function(e) {
    el.parentNode.classList.toggle('accordion__item--active');
    e.preventDefault();
  });
});
