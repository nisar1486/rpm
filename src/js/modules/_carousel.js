var Flickity = require('flickity');
require('flickity-imagesloaded')

var carousel = document.querySelectorAll('.carousel');
Array.prototype.forEach.call(carousel, function(el, i) {
  var flkty = new Flickity(el, {
    imagesLoaded: true,
    pageDots: false,
    freeScroll: true,
    wrapAround: true
  });
});
