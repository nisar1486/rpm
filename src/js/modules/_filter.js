var submit = document.querySelectorAll('.filter__submit');

var filter = function(e) {
  var url = '/teachers/';
  var location = 'los-angeles';
  var lesson = document.querySelector('#filter-lesson').value;
  if (location && lesson) {
    url = '/lessons/' + location + '/' + lesson + '/';
  } else if (location) {
    url = '/locations/' + location + '/';
  } else if (lesson) {
    url = '/lessons/' + lesson + '/';
  }
  this.setAttribute('href', url);
};

for (var i = 0; i < submit.length; i += 1) {
  submit[i].addEventListener('click', filter);
}

//

var update = function() {
  var text = this.options[this.selectedIndex].text;
  var container = this.parentNode.querySelector('.filter__select__text');
  container.innerHTML = text;
};

var selects = document.querySelectorAll('.filter__select select');

for (var i = 0; i < selects.length; i += 1) {
  update.call(selects[i]);
  selects[i].addEventListener('change', update);
}
