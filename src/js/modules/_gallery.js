var gallery = document.querySelectorAll('.gallery');
Array.prototype.forEach.call(gallery, function(el, i) {
  var gallery = el;
  var full = gallery.querySelector('.gallery__full');
  var nav = gallery.querySelectorAll('.gallery__nav__link');
  Array.prototype.forEach.call(nav, function(el, i) {
    var load = el.getAttribute('data-load');
    el.addEventListener('click', function(e) {
      var active =  gallery.querySelector('.gallery__nav__link--active');
      full.innerHTML = load;
      active.classList.remove('gallery__nav__link--active');
      el.classList.add('gallery__nav__link--active');
      e.preventDefault();
    });
  });
});

var active = document.querySelectorAll('.gallery__nav__link--active');

for (var i = 0; i < active.length; i += 1) {
  active[i].click();
}
