var toggle = document.querySelector('.header__nav-toggle');
var nav = document.querySelector('.header__nav');
toggle.addEventListener('click', function(e) {
  nav.classList.toggle('header__nav--active');
  e.preventDefault();
});

var navToggles = document.querySelectorAll('.menu-item-has-children > a');
for (var i = 0; i < navToggles.length; i += 1) {
  navToggles[i].addEventListener('click', function(e) {
    navClose(this.parentNode);
    this.parentNode.classList.toggle('sub-menu-active');
    e.preventDefault();
  });
}

function navClose(target) {
  document.addEventListener('click', close);
  function close() {
    if (!target.contains(event.target)) {
      target.classList.remove('sub-menu-active');
      document.removeEventListener('click', close);
    }
  }
}
