var more = document.querySelector('.instruments__more');
var instruments = document.querySelector('.instruments');
if (more) {
  more.addEventListener('click', function(e) {
    instruments.classList.add('instruments--active');
    e.preventDefault();
  });
}


var more = document.querySelector('.locations__more');
var locations = document.querySelector('.locations');
if (more) {
  more.addEventListener('click', function(e) {
    locations.classList.add('locations--active');
    e.preventDefault();
  });
}
