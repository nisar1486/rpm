var Flickity = require('flickity');
require('flickity-imagesloaded')

var reviews = document.querySelectorAll('.reviews__list');
Array.prototype.forEach.call(reviews, function(el, i) {
  var flkty = new Flickity(el, {
    imagesLoaded: true,
    pageDots: false,
    wrapAround: true
  });
});
