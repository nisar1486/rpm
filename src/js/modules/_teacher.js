var Flickity = require('flickity');
require('flickity-imagesloaded')

var testimonials = document.querySelectorAll('.teacher__testimonials');
Array.prototype.forEach.call(testimonials, function(el, i) {
  var flkty = new Flickity(el, {
    imagesLoaded: true,
    prevNextButtons: false,
    wrapAround: true,
    cellSelector: '.teacher__testimonials__item'
  });
});
