// customized modernizr build
require('./util/_modernizr');

// fastclick eliminates click delay in certain browsers
var fastclick = require('fastclick');
fastclick(document.body);

// svg polyfill for better xlink support
var svg4everybody = require('svg4everybody');
svg4everybody();

// modules
require('./modules/_accordion');
require('./modules/_carousel');
require('./modules/_filter');
require('./modules/_gallery');
require('./modules/_header');
require('./modules/_instruments');
require('./modules/_reviews');
require('./modules/_teacher');
