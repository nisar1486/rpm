<?php get_header(); ?>

<?php
$term_id = get_queried_object()->term_id;
global $wp_query;
query_posts(
	array_merge(
		$wp_query->query,	array(
  		'orderby'=>'menu_order',
  		'posts_per_page' => -1,
		)
	)
);
?>

<?php if (have_rows('before_content_modules', 'lesson_' . $term_id)) : ?>
  <?php while (have_rows('before_content_modules', 'lesson_' . $term_id)) : the_row(); ?>

    <?php get_template_part('modules/_' . get_row_layout()); ?>

  <?php endwhile; ?>
<?php endif; ?>

<a id="teachers"></a>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('modules/_teacher'); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('after_content_modules', 'lesson_' . $term_id)) : ?>
  <?php while (have_rows('after_content_modules', 'lesson_' . $term_id)) : the_row(); ?>

    <?php get_template_part('modules/_' . get_row_layout()); ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
